import React from "react";
import SearchBuilder from "./containers/SearchBuilder/SearchBuilder";
import {Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Serial from "./components/Serial/Serial";

const App = () => (
  <Layout>
      <Switch>
          <Route path="/" exact component={SearchBuilder} />
          <Route path="/shows/:id" component={Serial} />
          <Route render={() => <h1>Not found</h1>} />
      </Switch>
  </Layout>
);

export default App;
