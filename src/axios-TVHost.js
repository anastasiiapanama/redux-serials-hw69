import axios from "axios";

const axiosTVHost = axios.create({
   baseURL: 'http://api.tvmaze.com/'
});

export default axiosTVHost;