import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import './NavigationItems.css';

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/" exact>TV Shows Search</NavigationItem>
        </ul>
    );
};

export default NavigationItems;