import React, {useEffect} from 'react';
import {showSerial} from "../../store/SearchBuilderActions";
import {useDispatch, useSelector} from "react-redux";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: 800
    },
    media: {
        height: 140,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const Serial = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const state = useSelector(state => state.oneShow);
    const idSerial = props.match.params.id;

    useEffect(() => {
        dispatch(showSerial(idSerial));
    }, [dispatch]);

    return (
        <Paper className={classes.paper}>
            <Grid container direction="row" spacing={5}>
                <Grid item xs>
                    <Card className={classes.root}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" align="center">
                                    {state.name}
                                </Typography>
                                <Typography variant="body2" color="primary" component="p">
                                    {state.summary}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Year: {state.premiered}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Status: {state.status}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default Serial;