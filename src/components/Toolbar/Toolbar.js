import React from 'react';
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";
import './Toolbar.css';
import Grid from "@material-ui/core/Grid";

const Toolbar = () => {

    return (
        <header className="Toolbar">
            <Grid container spacing={5}>
                <Grid container item xs alignItems="center" direction="row">
                    <Grid item xs={4} spacing={3}>
                        <div className="Toolbar-logo">TV Shows</div>
                    </Grid>
                    <Grid item xs={8} spacing={3} alignContent="flex-end" alignItems="flex-end">
                        <nav><NavigationItems/></nav>
                    </Grid>
                </Grid>
            </Grid>
        </header>
    );
};

export default Toolbar;