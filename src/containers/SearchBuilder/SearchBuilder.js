import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import {fetchSeries, showSerial} from "../../store/SearchBuilderActions";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const SearchBuilder = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const state = useSelector(state => state.series);

    const [name, setName] = useState('');

    useEffect(() => {
        dispatch(fetchSeries(name));
    }, [name, dispatch]);

    return (
        <Paper className={classes.paper}>
            <Grid container direction="column" spacing={5}>
                <Grid container item xs alignItems="center" direction="row" spacing={6}>
                    <Grid item xs={4} spacing={3}>
                        <Typography variant="h5" align="center">TV Search:</Typography>
                    </Grid>
                    <Grid item xs={8} spacing={3}>
                        <TextField
                            fullWidth
                            variant="outlined"
                            type="text" name="text"
                            onChange={(e) => setName(e.target.value)}
                        />
                    </Grid>
                </Grid>
                <Grid container item xs alignItems="center" direction="column">
                    <Grid container item xs direction="column" spacing={3}>
                        {state.map(s => {
                            return (
                                <NavLink key={s.show.id} to={"/shows/" + s.show.id}>{s.show.name}</NavLink>
                            )
                        })}
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default SearchBuilder;