import axios from "axios";
import axiosTVHost from "../axios-TVHost";

export const FETCH_SERIES_REQUEST = 'FETCH_SERIES_REQUEST';
export const FETCH_SERIES_SUCCESS = 'FETCH_SERIES_SUCCESS';
export const FETCH_SERIES_FAILURE = 'FETCH_SERIES_FAILURE';
export const GET_ONE_SHOW = 'GET_ONE_SHOW';

export const fetchSeriesRequest = () => ({type: FETCH_SERIES_REQUEST});
export const fetchSeriesSuccess = series => ({type: FETCH_SERIES_SUCCESS, series});
export const fetchSeriesFailure = () => ({type: FETCH_SERIES_FAILURE});
export const showSeriesType = show => ({type: GET_ONE_SHOW, show});

export const fetchSeries = text => {
    return async dispatch => {
        dispatch(fetchSeriesRequest());

        try {
            const response = await axios.get("http://api.tvmaze.com/search/shows?q=" + text);

            dispatch(fetchSeriesSuccess(response.data));
        } catch (e) {
            dispatch(fetchSeriesFailure());
        }
    }
};

export const showSerial = id => {
    console.log(id)
    return async dispatch => {
        try {
            const response = await axiosTVHost.get("/shows/" + id);

            dispatch(showSeriesType(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

