import {FETCH_SERIES_FAILURE, FETCH_SERIES_REQUEST, FETCH_SERIES_SUCCESS, GET_ONE_SHOW} from "./SearchBuilderActions";

const initialState = {
    series: [],
    loading: true,
    error: false,
    oneShow: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SERIES_REQUEST:
            return {...state, loading: true, error: false};
        case FETCH_SERIES_SUCCESS:
            return {...state, loading: false, series: action.series};
        case FETCH_SERIES_FAILURE:
            return {...state, loading: false, error: true};
        case GET_ONE_SHOW:
            return {...state, oneShow: action.show, loading: false};
        default:
            return state;
    }
};

export default reducer;